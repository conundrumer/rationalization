function farey(n) {
  const out = [];

  let [a, b, c, d] = [0, 1, 1, n];

  out.push([a, b]);

  while (c <= n) {
    let k = Math.floor((n + b) / d);
    [a, b, c, d] = [c, d, k * c - a, k * d - b];
    out.push([a, b]);
  }

  return out;
}

function sbTree(n) {
  if (n === 0) return [[0, 1], [1, 1]];

  const out = [];

  let [prev, ...prevTree] = sbTree(n - 1);

  out.push(prev);
  for (let curr of prevTree) {
    out.push([prev[0] + curr[0], prev[1] + curr[1]]);
    out.push(curr);
    prev = curr;
  }

  return out;
}

function* genSbTree(
  n,
  parent = { r: [1, 2], cf: [2] },
  left = [0, 1],
  right = [1, 1]
) {
  if (parent.r[1] > n) {
    return;
  }

  const zig = [...parent.cf];
  zig[zig.length - 1]++;
  const zag = [...parent.cf, 2];
  zag[zag.length - 2]--;

  let cLeft, cRight;
  if (parent.cf.length % 2 === 1) {
    // moving left
    cLeft = zig;
    cRight = zag;
  } else {
    // moving right
    cLeft = zag;
    cRight = zig;
  }

  const nextLeft = {
    r: [parent.r[0] + left[0], parent.r[1] + left[1]],
    cf: cLeft
  };
  const nextRight = {
    r: [parent.r[0] + right[0], parent.r[1] + right[1]],
    cf: cRight
  };

  yield* genSbTree(n, nextLeft, left, parent.r);
  yield parent;
  yield* genSbTree(n, nextRight, parent.r, right);
}

function multiplyCf(cf, [a, b, c, d]) {
  for (let i = cf.length - 1; i >= 0; i--) {
    const k = cf[i];
    [a, b, c, d] = [c, d, a + c * k, b + d * k];
  }
  return [a, b, c, d];
}
const G = (1 + Math.sqrt(5)) / 2;

function* genRationals(n) {
  for (let { r, cf } of genSbTree(n)) {
    const ir = multiplyCf(cf, [1, -1, 0, 1]);
    yield {
      r,
      cf,
      ir,
      value: r[0] / r[1],
      irValue: (ir[0] * G + ir[1]) / (ir[2] * G + ir[3])
    };
  }
}

function fibs(n) {
  const tmp = [];

  for (let a = -n; a <= n; a++) {
    for (let b = -n; b <= n; b++) {
      for (let c = -n; c <= n; c++) {
        for (let d = -n; d <= n; d++) {
          if (Math.abs(a * d - b * c) != 1) continue;

          let value = (a * G + b) / (c * G + d);
          value = ((value % 1) + 1) % 1;
          if (value > 0.5) {
            value = 1 - value;
          }
          tmp.push({
            coeffs: [a, b, c, d],
            value
          });
        }
      }
    }
  }

  tmp.sort((a, b) => a.value - b.value);

  const out = [];
  let curr = tmp.shift();
  let x0 = curr.value;
  for (let next of tmp) {
    if (next.value - x0 > 1e-10) {
      // go to next interval
      out.push(curr);
      curr = next;
      x0 = curr.value;
    } else {
      // in same interval, pick more optimal value
      if (
        absSum(next) <= absSum(curr) &&
        signSum(next) <= signSum(curr) &&
        zeroSum(next) >= zeroSum(curr) &&
        zeroACSum(next) >= zeroACSum(curr) &&
        demPos(next) >= demPos(curr)
      ) {
        curr = next;
      }
    }
  }
  out.push(curr);

  return out;

  function absSum(entry) {
    return entry.coeffs.reduce((a, b) => a + Math.abs(b), 0);
  }
  function signSum(entry) {
    return entry.coeffs.reduce((a, b) => a + (b < 0 ? 1 : 0), 0);
  }
  function zeroSum(entry) {
    return entry.coeffs.reduce((a, b) => a + (b === 0 ? 1 : 0), 0);
  }
  function zeroACSum(entry) {
    return (entry.coeffs[0] === 0 ? 1 : 0) + (entry.coeffs[2] === 0 ? 2 : 0);
  }
  function demPos(entry) {
    return (entry.coeffs[2] >= 0 ? 1 : 0) + (entry.coeffs[3] >= 0 ? 1 : 0);
  }
}

// https://stackoverflow.com/a/29018745/2573317
function binarySearch(ar, el, compare_fn) {
  var m = 0;
  var n = ar.length - 1;
  while (m <= n) {
    var k = (n + m) >> 1;
    var cmp = compare_fn(el, ar[k]);
    if (cmp > 0) {
      m = k + 1;
    } else if (cmp < 0) {
      n = k - 1;
    } else {
      return k;
    }
  }
  return m;
}
function gcd(a, b) {
  if (!b) {
    return a;
  }

  return gcd(b, a % b);
}

const BASE = 1.9
const KEY_MAP = {
  "0": 0,
  "1": 1,
  "2": Math.pow(BASE, -1),
  "3": Math.pow(BASE, -2),
  "4": Math.pow(BASE, -3),
  "5": Math.pow(BASE, -4),
  "6": Math.pow(BASE, -5),
  "7": Math.pow(BASE, -6),
  "8": Math.pow(BASE, -7),
  "9": Math.pow(BASE, -8),
  ")": 0,
  "!": -1,
  "@": -Math.pow(BASE, -1),
  "#": -Math.pow(BASE, -2),
  "$": -Math.pow(BASE, -3),
  "%": -Math.pow(BASE, -4),
  "^": -Math.pow(BASE, -5),
  "&": -Math.pow(BASE, -6),
  "*": -Math.pow(BASE, -7),
  "(": -Math.pow(BASE, -8)
};
