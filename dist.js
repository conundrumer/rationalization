distInit();

function distInit() {
  const canvas = document.getElementsByTagName("canvas")[0];
  const ctx = canvas.getContext("2d");
  canvas.width = window.devicePixelRatio * canvas.clientWidth;
  canvas.height = window.devicePixelRatio * canvas.clientHeight;
  const W = canvas.width;
  const H = canvas.height;

  let i = 3;
  let timer;
  function draw() {
    ctx.clearRect(0, 0, W, H);

    const values = [...genRationals(i)]
    const rs = values.map(({r, value}) => [value, 1 / r[1]])
    const irs = values.map(({r, irValue, cf}) => [irValue, 1 / (r[1] + 1)])

    ctx.strokeStyle = 'blue'
    // let irrationals = fibs(i - 2);
    // irrationals = [
    //   ...irrationals,
    //   ...irrationals
    //     .reverse()
    //     .map(({ coeffs, value }) => ({ coeffs, value: 1 - value }))
    // ];
    // plotGraph(
    //   irrationals.map(({ value }) => value),
    //   Math.sqrt(irrationals.length)
    // );
    // plotLines(
    //   irrationals.map(({ coeffs, value }) => [
    //     value,
    //     0.5 / Math.max(...coeffs.map(x => Math.abs(x)))
    //   ])
    // );
    plotLines(irs)

    ctx.strokeStyle = 'red'
    // const rationals = sbTree(i);
    // plotGraph(
    //   rationals.map(([p, q]) => p / q),
    //   2 * Math.pow(rationals.length, 1/ 4)
    // );
    // const rationals = farey(i);
    // plotLines(rationals.map(([p, q]) => [p / q, 1 / q]));
    plotLines(rs)

    i++;
    if (i > 50) return;
    // if (i > 1000) return;

    timer = setTimeout(draw, 1000 / 30);
  }
  draw();

  window.addEventListener("click", () => {
    clearTimeout(timer);
    i = 3;
    draw();
  });

  function plotGraph(ys, dyScale) {
    const N = ys.length;

    ctx.beginPath();
    ctx.moveTo(0, 0);

    for (let [i, y] of ys.entries()) {
      const x = i / (N - 1);
      ctx.lineTo(x * W, y * H);
    }

    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(0, 0);

    ys = [...ys];
    let prevY = ys.shift();
    for (let [i, y] of ys.entries()) {
      const x = i / (N - 2);
      const dy = (y - prevY) * dyScale;
      prevY = y;
      ctx.lineTo(x * W, dy * H);
    }

    ctx.stroke();
  }

  function plotLines(ys) {
    ctx.beginPath();

    for (let [x, y] of ys) {
      ctx.moveTo(x * W, 0);
      ctx.lineTo(x * W, y * H);
    }

    ctx.stroke();
  }
}
