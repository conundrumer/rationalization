/* vars */
const N = 1000;
const scrollHeight = 600000;
const maxDem = 99;
// const sizeK = 2.5;

let exactPos;
let scrollPos = 0;
let angle = 0;
let timer;
let scrollTimer;
let resizeTimer;
const makeFracArgs = [];

/* webgl setup */
const square = [[-0.5, 0], [0.5, 0], [-0.5, 1], [-0.5, 1], [0.5, 0], [0.5, 1]];
const squares = [];
const indices = [];
for (let i = 0; i < N; i++) {
  for (let pos of square) {
    squares.push(pos);
    indices.push(i);
  }
}

// @ts-ignore
const regl = window.createREGL({
  // pixelRatio: 1
});

const draw = regl({
  frag: `
    precision mediump float;
    void main() {
      gl_FragColor = vec4(0,0,0,0.25);
    }`,

  vert: `
    precision highp float;
    uniform float angle;
    uniform float width;
    uniform float height;
    attribute vec2 position;
    attribute float index;
    const float N = ${N.toFixed(1)};

    vec2 rotate(vec2 v, float a) {
      float s = sin(a);
      float c = cos(a);
      mat2 m = mat2(c, -s, s, c);
      return m * v;
    }

    void main() {
      float r = length(vec2(width, height));
      float r0 = r * sqrt(index / N);
      vec2 pos = position;
      pos = pos * vec2(3., r - r0) + vec2(0, r0);
      pos = rotate(pos, angle * index * 6.283185307179586);
      pos = pos / vec2(width, height);
      gl_Position = vec4(pos, -index / N, 1);
    }`,

  attributes: {
    position: squares,
    index: indices
  },

  uniforms: {
    width: regl.context("viewportWidth"),
    height: regl.context("viewportHeight"),
    angle: regl.prop("angle")
  },

  blend: {
    enable: true,
    func: {
      srcRGB: "src alpha",
      srcAlpha: 1,
      dstRGB: "one minus src alpha",
      dstAlpha: 1
    }
  },

  count: N * square.length
});

/* DOM setup */
const canvas = document.getElementsByTagName("canvas")[0];
canvas.style.position = "fixed";

const scrollContainer = document.getElementById("scroll-container");
document.body.appendChild(scrollContainer);

// scrollContainer.style.setProperty("--scroll-height", scrollHeight + "px");
const timerStart = Date.now()
makeFrac(0, null, 0.5, 0);
makeFrac(1, null, 0.5, 1);
makeFrac(makePhi(0, 1), makePhi(1, 0), 0, 1 / G, 0.35 + 3, 1 / G);

for (let r of genRationals(maxDem)) {
  let x = (r.r[1] - 2) / (maxDem - 2);
  x = 1 - Math.pow(1 - x, 2);
  const z1 = Math.pow(1 - x, 1.5) * 8 + 0.8;
  const z2 = Math.pow(1 - x, 1.5) * 1.5 + 0.5;

  const args1 = [r.r[0], r.r[1], x, r.value, z1, r.value];
  const args2 = [
    makePhi(r.ir[0], r.ir[1]),
    makePhi(r.ir[2], r.ir[3]),
    x,
    r.irValue,
    z2,
    r.irValue
  ];
  // if (r.r[1] < 20) {
    makeFrac(...args1);
    makeFrac(...args2);
  // } else {
    makeFracArgs.push(args1);
    makeFracArgs.push(args2);
  // }
}
// setTimeout(() => {
//   scrollContainer.remove()
//   for (let arg of makeFracArgs) {
//     makeFrac(...arg)
//   }
//   makeFracArgs.length = 0
//   document.body.append(scrollContainer)
// }, 500)
// while (Date.now() - timerStart < 500 && makeFracArgs.length > 0) {
//   makeFrac(...makeFracArgs.shift())
// }
makeFracArgs.sort((argsA, argsB) => argsA[3] - argsB[3]);
// const demArgs = [...makeFracArgs].sort((argsA, argsB) => argsA[2] - argsB[2]);

// window.requestIdleCallback(slowlyAddByDem);
// function slowlyAddByDem(deadline) {
//   let i = 0
//   while (deadline.timeRemaining() > 0) {
//     let next;
//     let index = -1
//     while (index === -1) {
//       next = demArgs.shift();
//       if (!next) {
//         console.log('done adding fracs')
//         return;
//       }
//       index = makeFracArgs.indexOf(next)
//       if (index === -1) {
//         // already inserted
//         continue;
//       }
//     }
//     if (index === -1) {
//       console.log('no more fracs')
//       return
//     }
//     makeFracArgs.splice(index, 1)
//     makeFrac(...next);
//     i++
//   }
//   // console.log(i)

//   window.requestIdleCallback(slowlyAddByDem);
// }

function makeFrac(p, q, x, y, z, value) {
  const div = document.createElement("div");
  scrollContainer.appendChild(div);

  div.classList.add("frac");
  div.classList.add("hidden");
  div.style.setProperty("--x", x);
  div.style.setProperty("--y", y);
  if (z) {
    div.style.setProperty("--z", z);
  } else {
    div.classList.add("big");
  }
  if (value != null) {
    div.setAttribute("data-value", value);
  }

  if (q == null) {
    div.innerHTML = p;
  } else {
    const top = document.createElement("div");
    const bottom = document.createElement("div");
    div.appendChild(top);
    div.appendChild(bottom);
    top.innerHTML = p;
    bottom.innerHTML = q;
  }

  return div;
}

function makePhi(a, b) {
  if (a === 0) {
    return b;
  }
  let out = "";
  if (a === 1) {
    out += "<i>ɸ</i>";
  } else {
    out += `${a}<i>ɸ</i>`;
  }
  if (b > 0) {
    out += `＋${b}`;
  }
  return out;
}

/* event setup */

updateScroll();
onFrame(true);

window.addEventListener("scroll", () => {
  updateScroll();
  if (timer == null) {
    onFrame();
  }
  if (exactPos != null) {
    clearTimeout(scrollTimer);
    scrollTimer = setTimeout(() => {
      scrollPos = exactPos;
      exactPos = null;
    }, 200);
  }
});
function onManualScroll() {
  clearTimeout(scrollTimer);
  exactPos = null;
}
window.addEventListener("wheel", onManualScroll);
window.addEventListener("touchmove", onManualScroll);
scrollContainer.addEventListener("click", e => {
  const data =
    e.target.getAttribute("data-value") ||
    e.target.parentNode.getAttribute("data-value");
  if (data) {
    const value = parseFloat(data);
    exactPos = value;
    window.scroll({ top: scrollHeight * value, behavior: "smooth" });
  }
});
window.addEventListener("resize", () => {
  clearTimeout(resizeTimer);
  if (timer == null) {
    resizeTimer = setTimeout(() => {
      onFrame(true);
    }, 200);
  }
});

function updateScroll() {
  scrollPos = window.pageYOffset / scrollHeight;
  if (makeFracArgs.length === 0) return;

  const scrollPosByWindow = window.pageYOffset / window.innerHeight
  const start = ((Math.floor(scrollPosByWindow) - 5) * window.innerHeight) / scrollHeight;
  const end = ((Math.ceil(scrollPosByWindow) + 5) * window.innerHeight) / scrollHeight;
  const startIndex = binarySearch(
    makeFracArgs,
    start,
    (x, args) => x - args[3]
  );
  let arg = makeFracArgs[startIndex];

  let i = 0
  while (arg && arg[3] < end) {
    // console.log(...arg);
    // makeFrac(...arg);
    const node = document.querySelector(`[data-value="${arg[5]}"]`)
    if (!node) {
      debugger
    }
    node.classList.remove('hidden')
    makeFracArgs.splice(startIndex, 1);
    arg = makeFracArgs[startIndex];
    i++
  }
  console.log(i)
}

function onFrame(force) {
  if (force !== true && scrollPos === angle) {
    timer = null;
    return;
  } else if (Math.abs(scrollPos - angle) < 1e-7) {
    angle = scrollPos;
  } else {
    angle = angle + 0.2 * (scrollPos - angle);
  }

  regl.poll();
  // clear contents of the drawing buffer
  regl.clear({
    color: [0, 0, 0, 0],
    depth: 1
  });

  // draw a triangle using the command defined above
  draw({
    angle
  });

  timer = requestAnimationFrame(onFrame);
}
