const N = 1000;
const SCROLL_HEIGHT = 600000;
const MAX_DEM = 99;
const CSSClasses = {
  Number: "n",
  Big: "big",
  Fraction: "f",
  Rational: "r",
  Irrational: "i",
};

class Scroller {
  constructor(scrollHeight, k) {
    this.scrollHeight = scrollHeight;
    this.k = k;
    this.smoothedPos = 0;
    this.pos = 0;
    this.onScroll();
  }
  getScrollPos() {
    return this.autoscrollPos != null ? this.autoscrollPos : this.pos;
  }
  autoScrollTo(pos, smooth = true) {
    this.autoscrollPos = pos;

    this.handleScroll = () => {
      clearTimeout(this.autoScrollTimer);
      this.autoScrollTimer = setTimeout(this.onAutoScrollEnd, 1000 / 30);
    };
    this.onAutoScrollEnd = () => {
      this.pos = pos;
      this.handleScroll = null;
    };
    window.scroll({
      top: this.scrollHeight * pos,
      behavior: smooth ? "smooth" : undefined,
    });
    this.handleScroll();
  }
  autoplay(speed, fromTheTop) {
    if (this.cancelAutoplay) {
      this.cancelAutoplay();
    }
    if (speed === 0 || speed == null) return;

    let delay = 0;
    if (fromTheTop) {
      delay = 500;

      window.scroll({ top: 0 });
      this.pos = 0;
      this.autoscrollPos = 0;
      this.smoothedPos = 1e-6;
    }

    let t1 = setTimeout(() => {
      let y = window.scrollY;
      let prevTime = performance.now();
      this.handleScroll = () => {
        if ((speed > 0 && this.pos >= 1) || (speed < 0 && this.pos <= 0)) {
          this.handleScroll = null;
          return;
        }
        const time = performance.now();
        const delta = (time - prevTime) / 1000 / 60;
        prevTime = time;

        let dy = this.scrollHeight * delta * speed;

        if (Math.abs(dy) < 1) {
          dy = Math.sign(dy);
        }
        dy = Math.round(dy);
        y += dy;
        window.scroll({ top: y });
        requestAnimationFrame(() => this.handleScroll?.());
      };
      this.handleScroll()
    }, delay);
    this.cancelAutoplay = () => {
      this.cancelAutoplay = null;
      clearTimeout(t1);
      this.handleScroll = null;
    };
  }
  onManualScroll() {
    clearTimeout(this.autoScrollTimer);
    this.autoscrollPos = null;
  }
  onScroll() {
    this.pos = window.scrollY / this.scrollHeight;
  }
  onFrame() {
    if (this.pos === this.smoothedPos) {
      return false;
    }
    if (Math.abs(this.pos - this.smoothedPos) < 1e-8) {
      this.smoothedPos = this.pos;
    } else {
      this.smoothedPos =
        this.smoothedPos + this.k * (this.pos - this.smoothedPos);
    }
    return true;
  }
}

class AbstractNumber {
  constructor(value) {
    this.div = document.createElement("div");
    this.value = value;
  }
  addContents() {
    this.div.style.setProperty("--y", this.value);
  }
}
class WholeNumber extends AbstractNumber {
  addContents() {
    super.addContents();
    this.div.classList.add(CSSClasses.Number, CSSClasses.Big);
    this.div.style.setProperty("--x", "0.5");
    this.div.textContent = this.value;
  }
}
class RationalNumber extends AbstractNumber {
  constructor(value, [p, q]) {
    super(value);
    this.p = p;
    this.q = q;
  }
  addContents() {
    super.addContents();
    this.div.classList.add(
      CSSClasses.Number,
      CSSClasses.Fraction,
      CSSClasses.Fraction + this.q,
      CSSClasses.Rational + this.q
    );
    const top = this.div.appendChild(document.createElement("div"));
    const bottom = this.div.appendChild(document.createElement("div"));
    top.textContent = this.p;
    bottom.textContent = this.q;
  }
}
class IrrationalNumber extends AbstractNumber {
  constructor(value, [p, q], coeffs) {
    super(value);
    this.p = p;
    this.q = q;
    this.coeffs = coeffs;
  }
  _makePhi(div, a, b) {
    super.addContents();
    if (a === 0) {
      div.textContent = b;
      return;
    }
    const children = [];
    const phi = document.createElement("i");
    phi.textContent = "ɸ";

    if (a !== 1) {
      children.push(a);
    }
    children.push(phi);
    if (b !== 0) {
      children.push(`＋${b}`);
    }
    div.append(...children);
  }
  addContents() {
    this.div.classList.add(
      CSSClasses.Number,
      CSSClasses.Fraction,
      CSSClasses.Irrational,
      CSSClasses.Fraction + this.q,
      CSSClasses.Irrational + this.q
    );
    const top = this.div.appendChild(document.createElement("div"));
    const bottom = this.div.appendChild(document.createElement("div"));
    this._makePhi(top, this.coeffs[0], this.coeffs[1]);
    this._makePhi(bottom, this.coeffs[2], this.coeffs[3]);
  }
}
class NumberManager {
  /**
   * @param {Scroller} scroller
   */
  constructor(numDems, scroller) {
    this.scroller = scroller;
    const numbers = [];
    numbers.push(
      new WholeNumber(0),
      new WholeNumber(1),
      new IrrationalNumber(1 / G, [0, 2], [0, 1, 1, 0])
    );
    for (let number of genRationals(numDems)) {
      numbers.push(
        new RationalNumber(number.value, number.r),
        new IrrationalNumber(number.irValue, number.r, number.ir)
      );
    }
    numbers.sort((a, b) => a.value - b.value);
    this.divNumberMap = new WeakMap();

    for (let number of numbers) {
      this.divNumberMap.set(number.div, number);
    }
    this.numbers = numbers;
    this.hiddenNumbers = [...numbers];
  }

  /**
   * @param {HTMLElement} container
   */
  setupContainer(container) {
    for (let number of this.numbers) {
      container.appendChild(number.div);
    }
    container.addEventListener("click", (e) => {
      let target = e.target;
      let number;
      while (target && target !== container) {
        number = this.divNumberMap.get(target);
        target = target.parentNode;
      }
      if (number) {
        this.scrollToNumber(number);
      }
    });
  }
  /**
   * @param {AbstractNumber} number
   */
  scrollToNumber(number) {
    this.scroller.autoScrollTo(number.value);
  }

  scrollToNextValue(reverse) {
    const value = this.scroller.getScrollPos();
    let i;
    for (i = 0; i < this.numbers.length; i++) {
      const nextValue = this.numbers[i].value;
      if (nextValue === value) {
        i += reverse ? -1 : 1;
        break;
      }
      if (nextValue > value) {
        if (reverse) {
          i--;
        }
        break;
      }
    }
    i = Math.max(0, Math.min(this.numbers.length - 1, i));

    this.scrollToNumber(this.numbers[i]);
  }

  handleScroll() {
    const scrollPosByWindow = window.scrollY / window.innerHeight;

    const startValue =
      ((Math.floor(scrollPosByWindow) - 3) * window.innerHeight) /
      SCROLL_HEIGHT;
    const endValue =
      ((Math.ceil(scrollPosByWindow) + 3) * window.innerHeight) / SCROLL_HEIGHT;

    const startIndex = binarySearch(
      this.hiddenNumbers,
      startValue,
      (x, number) => x - number.value
    );
    let endIndex;
    for (
      endIndex = startIndex;
      endIndex < this.hiddenNumbers.length &&
      this.hiddenNumbers[endIndex].value < endValue;
      endIndex++
    ) {}
    if (endIndex > startIndex) {
      const numbersToAdd = this.hiddenNumbers.splice(
        startIndex,
        endIndex - startIndex
      );
      for (let number of numbersToAdd) {
        number.addContents();
      }
    }
  }
  addAllHidden() {
    for (let number of this.hiddenNumbers) {
      number.addContents();
    }
    this.hiddenNumbers.length = 0;
  }
}
addFractionCSS(MAX_DEM);

const scrollContainer = document.getElementById("scroll-container");

const scroller = new Scroller(SCROLL_HEIGHT, 0.2);

const numberManager = new NumberManager(MAX_DEM, scroller);

numberManager.setupContainer(scrollContainer);

// @ts-ignore
const regl = window.createREGL();

// update DOM after regl inserts canvas
document.getElementsByTagName("canvas")[0].style.position = "fixed";
document.body.appendChild(scrollContainer);

const draw = (() => {
  const square = [
    [-0.5, 0],
    [0.5, 0],
    [-0.5, 1],
    [-0.5, 1],
    [0.5, 0],
    [0.5, 1],
  ];
  const squares = [];
  const indices = [];
  for (let i = 0; i < N; i++) {
    for (let pos of square) {
      squares.push(pos);
      indices.push(i);
    }
  }

  return regl({
    frag: `
    precision mediump float;
    void main() {
      gl_FragColor = vec4(0,0,0,0.25);
    }`,

    vert: `
    precision highp float;
    uniform float angle;
    uniform float width;
    uniform float height;
    attribute vec2 position;
    attribute float index;
    const float N = ${N.toFixed(1)};

    vec2 rotate(vec2 v, float a) {
      float s = sin(a);
      float c = cos(a);
      mat2 m = mat2(c, -s, s, c);
      return m * v;
    }

    void main() {
      float r = length(vec2(width, height));
      float r0 = r * sqrt(index / N);
      vec2 pos = position;
      pos = pos * vec2(3., r - r0) + vec2(0, r0);
      pos = rotate(pos, angle * index * 6.283185307179586);
      pos = pos / vec2(width, height);
      gl_Position = vec4(pos, -index / N, 1);
    }`,

    attributes: {
      position: squares,
      index: indices,
    },

    uniforms: {
      width: regl.context("viewportWidth"),
      height: regl.context("viewportHeight"),
      angle: regl.prop("angle"),
    },

    blend: {
      enable: true,
      func: {
        srcRGB: "src alpha",
        srcAlpha: 1,
        dstRGB: "one minus src alpha",
        dstAlpha: 1,
      },
    },

    count: N * square.length,
  });
})();

const run = (() => {
  let tick;
  return function run(force) {
    if (tick != null) return;

    tick = regl.frame(() => {
      const updated = scroller.onFrame();

      if (!force && !updated) {
        tick.cancel();
        tick = null;
        return;
      }
      force = false;
      render();
    });
  };
})();

render();
run();
numberManager.handleScroll();

window.addEventListener("scroll", () => {
  scroller.onScroll();
  numberManager.handleScroll();
  run();
});
const onManualScroll = () => {
  scroller.onManualScroll();
};
window.addEventListener("wheel", onManualScroll);
window.addEventListener("touchmove", onManualScroll);
window.addEventListener("mousedown", onManualScroll);
window.addEventListener("keydown", (e) => {
  if (e.key in KEY_MAP) {
    scroller.autoplay(KEY_MAP[e.key]);
    return;
  }
  switch (e.key) {
    case "Escape":
      scroller.autoplay(0);
      break;
    case "Tab":
      e.preventDefault();
      scroller.autoplay(0);
      numberManager.scrollToNextValue(e.shiftKey);
      break;
    case "Home":
    case "End":
      e.preventDefault();
      scroller.autoplay(0);
      const pos = e.key === "Home" ? 0 : 1;
      numberManager.addAllHidden();
      scroller.autoScrollTo(pos);
      break;
    default:
      scroller.onManualScroll();
  }
});

void (() => {
  let timer;
  const onDebouncedResize = () => {
    run(true);
  };
  window.addEventListener("resize", () => {
    cancelAnimationFrame(timer);
    timer = requestAnimationFrame(onDebouncedResize);
  });
})();

window.addEventListener("hashchange", handleHash);
setTimeout(handleHash, 300);

function handleHash() {
  scroller.autoplay(0);
  const hash = location.hash.replace("#", "");
  let value;
  if (hash.startsWith("auto")) {
    let duration = parseFloat(hash.replace("auto", ""));
    if (!(duration > 0)) {
      duration = 1;
    }
    scroller.autoplay(1 / duration, true);
    setTimeout(() => numberManager.addAllHidden(), 200);
  } else if (hash === "phi") {
    value = 1 / G;
  } else if (hash.startsWith("phi:")) {
    let [p, q] = hash
      .replace("phi:", "")
      .split("/")
      .map((x) => parseInt(x));
    if (p === 0) {
      value = 1 / G;
    } else {
      const g = gcd(p, q);
      p /= g;
      q /= g;
      value = -1;
      for (let number of numberManager.numbers) {
        if (
          number instanceof IrrationalNumber &&
          number.p === p &&
          number.q === q
        ) {
          value = number.value;
        }
      }
    }
  } else if (hash.includes("/")) {
    const [p, q] = hash.split("/").map((x) => parseInt(x));
    value = p / q;
  } else {
    value = parseFloat(hash);
  }
  if (value !== 1) {
    value = ((value % 1) + 1) % 1;
  }
  if (value >= 0 && value <= 1) {
    setTimeout(() => {
      scroller.autoScrollTo(value, false);
    });
  }
}

function render() {
  regl.clear({
    color: [0, 0, 0, 0],
    depth: 1,
  });

  draw({ angle: scroller.smoothedPos });
}

function addFractionCSS(numDems) {
  let rules = "";
  for (let i = 2; i <= numDems; i++) {
    let x = (i - 2) / (numDems - 2);
    x = 1 - Math.pow(1 - x, 2);
    const rScale = Math.pow(1 - x, 1.5) * 8 + 0.8;
    const iScale = Math.pow(1 - x, 1.5) * 1.5 + 0.5;

    rules += `.${CSSClasses.Fraction}${i}{--x:${x};}`;
    rules += `.${CSSClasses.Rational}${i}{--z:${rScale};}`;
    rules += `.${CSSClasses.Irrational}${i}{--z:${iScale};}`;
  }
  const css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = rules;
  document.head.appendChild(css);
}
